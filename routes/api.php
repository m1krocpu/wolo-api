<?php

use Illuminate\Http\Request;

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/

Route::group(['middleware'=>'api'],function(){
	Route::post('v1/version', ['middleware' => 'cors',function() {
	    $data['success'] = 1;
	    $data['version'] = 21;
           
        return Response::json($data, '200');
	}]);

	Route::get('v1/', ['middleware' => 'cors',function() {return "";}]);
	Route::post('v1/oauth/authorize',['middleware' => 'cors','uses'=>'Api\v1\AuthController@authentication']);
	Route::post('v1/oauth/token',['middleware' => 'cors','uses'=>'Api\v1\AuthController@refresh']);
	Route::post('v1/oauth/nik',['middleware' => 'cors','uses'=>'Api\v1\AuthController@nik']);
	Route::post('v1/oauth/register',['middleware' => 'cors','uses'=>'Api\v1\AuthController@register']);
	Route::post('v1/oauth/registernik',['middleware' => 'cors','uses'=>'Api\v1\AuthController@registernik']);
	Route::post('v1/desa', ['middleware' => 'cors','uses'=>'ApiMobileController@desa']);

			
	Route::group(['middleware'=>'authapi'],function(){	
		//GET
		Route::post('v1/kuis', ['middleware' => 'cors','uses'=>'ApiMobileController@kuis']);
		Route::post('v1/duel', ['middleware' => 'cors','uses'=>'ApiMobileController@duel']);
		Route::post('v1/statistik', ['middleware' => 'cors','uses'=>'ApiMobileController@statistik']);
		Route::post('v1/kuisWinner', ['middleware' => 'cors','uses'=>'ApiMobileController@kuisWinner']);

		//STORE
		Route::post('v1/obrolan', ['middleware' => 'cors','uses'=>'ApiMobileController@Obrolan']);
		Route::post('v1/setKuis', ['middleware' => 'cors','uses'=>'ApiMobileController@setKuis']);
		Route::post('v1/setDuel', ['middleware' => 'cors','uses'=>'ApiMobileController@setDuel']);		
		Route::post('v1/joinDuel', ['middleware' => 'cors','uses'=>'ApiMobileController@joinDuel']);		
		Route::post('v1/submit', ['middleware' => 'cors','uses'=>'ApiMobileController@jawabDuel']);	

		//WEBVIEW	
		Route::get('v1/event', 'ApiMobileController@event');	
		Route::get('v1/news', 'ApiMobileController@news');	
		Route::get('v1/faqs', 'ApiMobileController@faqs');	
		Route::get('v1/profile', 'ApiMobileController@profile');	
		Route::get('v1/post/{id}', 'ApiMobileController@post_show');
	});
});


