<!DOCTYPE HTML>
<html>
	<head>
		<title></title>
		<meta name="viewport" content="width=device-width, user-scalable=no" />
		<link rel="stylesheet" href="{{url('/')}}/assets/css/style.css"/>
		<link rel="stylesheet" href="{{url('/')}}/assets/css/ionicons.min.css"/>
	</head>
	<body>
		<div class="detail-panel">
			<div class="card">
				<div class="panel-body" style="padding:0 30px;">
					<h1 class="main-title">
						{{$data->post_judul}}
					</h1>
				</div>
				<div class="panel-body" style="padding-top:0;">
					<div class="panel-body-user">
						<div class="media media-middle">
							<div class="media-body media-middle">
								<div>
									<a href="#">
										<small class="text-muted">{{date_format(date_create($data->post_created),"d/m/Y H:i")}}</small>
									</a>
								</div>
							</div>
						</div>
					</div>	
				</div>
				<div class="panel panel-detail">
					<article class="article-text">
						{!! $data->post_ket !!}
					</article>
				</div>
			</div>
		</div>
	</body>
</html>