<!DOCTYPE HTML>
<html>
	<head>
		<title></title>
		<meta name="viewport" content="width=device-width, user-scalable=no" />
		<link rel="stylesheet" href="{{url('/')}}/assets/css/style.css"/>
		<link rel="stylesheet" href="{{url('/')}}/assets/css/ionicons.min.css"/>
	</head>
	<body>
	<div class="faqs">
		<div class="faqs-body">
		<?php
			if ($data){
				foreach($data as $d){
		?>
			<div class="faqs-questions">
				<h4 class="pull-left">Q :</h4>
				<p>
					{{$d->faqs_judul}}
				</p>
			</div>
			<div class="faqs-answers">
				<h4 class="pull-left">A :</h4>
				<p>
					{{$d->faqs_ket}}
				</p>
			</div>
		<?php			
				}
			}else{
		?>
			<div class="news-empty">
				<img src="<?php echo url("/")?>/img/not-found.PNG" alt="news-empty"/>
				<p>FAQs Belum Tersedia</p>	
			</div>
		<?php
			}
		?>	
		</div>
	</div>
	</body>
</html>