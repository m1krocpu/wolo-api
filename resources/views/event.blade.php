<!DOCTYPE HTML>
<html>
	<head>
		<title></title>
		<meta name="descriptions" content=""/>
		<meta name="keywords" content=""/>
		<meta name="author" content=""/>
		<link rel="stylesheet" href="{{url('/')}}/assets/css/style.css"/>
		<link rel="stylesheet" href="{{url('/')}}/assets/css/ionicons.min.css"/>
	</head>
	<body>
		<div class="detail-panel">
		<?php
			if ($data){
				foreach($data as $d){
		?>
			<a class="card" href="<?php echo url("/")."/api/v1/post/".$d->post_id."?access_token=".$token ?>">
				<div class="panel-body" style="padding-bottom:0;">
					<small class="text-muted">{{date_format(date_create($d->post_created),"d/m/Y H:i:s")}}</small>
				</div>
				<div class="card-content" style="padding-top:0;">
					<h4>{{$d->post_judul}}</h4>
					<hr>
				</div>
			</a>
		<?php			
				}
			}else{
		?>
			<div class="news-empty">
				<img src="<?php echo url("/")?>/img/not-found.PNG" alt="news-empty"/>
				<p>Event Belum Tersedia</p>	
			</div>
		<?php
			}
		?>			
		</div>
	</body>
</html>