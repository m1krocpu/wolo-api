<!DOCTYPE HTML>
<html>
	<head>
		<title></title>
		<meta name="viewport" content="width=device-width, user-scalable=no" />
		<link rel="stylesheet" href="{{url('/')}}/assets/css/style.css"/>
		<link rel="stylesheet" href="{{url('/')}}/assets/css/ionicons.min.css"/>
	</head>
	<body>
		<div id="containter-content">
			<div class="statistik-dashboard-area kelurahan">
				<h1>Kelurahan / Desa</h1>
				<div class="tbl-header-statistik">
					<table cellpadding="0" cellspacing="0" border="0">
					  <thead>
						<tr>
						  <th>Peringkat</th>
						  <th>Kelurahan/Desa</th>
						  <th>Main</th>
						  <th>Menang</th>
						  <th>Seri</th>
						  <th>Kalah</th>
						  <th>Point</th>
						</tr>
					  </thead>
					</table>
				</div>
				<div class="tbl-content-statistik">
					<table cellpadding="0" cellspacing="0" border="0">
						<tbody>
							<tr>
							  <td>1</td>
							  <td>xxxxxx</td>
							  <td>2000</td>
							  <td>1000</td>
							  <td>1000</td>
							  <td>1000</td>
							  <td>1000</td>
							</tr>
							<tr>
							  <td>2</td>
							  <td>xxxxxx</td>
							  <td>2000</td>
							  <td>1000</td>
							  <td>1000</td>
							  <td>1000</td>
							  <td>1000</td>
							</tr>
							<tr>
							  <td>3</td>
							  <td>xxxxxx</td>
							  <td>2000</td>
							  <td>1000</td>
							  <td>1000</td>
							  <td>1000</td>
							  <td>1000</td>
							</tr>
							<tr>
							  <td>4</td>
							  <td>xxxxxx</td>
							  <td>2000</td>
							  <td>1000</td>
							  <td>1000</td>
							  <td>1000</td>
							  <td>1000</td>
							</tr>
							<tr>
							  <td>5</td>
							  <td>xxxxxx</td>
							  <td>2000</td>
							  <td>1000</td>
							  <td>1000</td>
							  <td>1000</td>
							  <td>1000</td>
							</tr>
							<tr>
							  <td>6</td>
							  <td>xxxxxx</td>
							  <td>2000</td>
							  <td>1000</td>
							  <td>1000</td>
							  <td>1000</td>
							  <td>1000</td>
							</tr>
							<tr>
							  <td>7</td>
							  <td>xxxxxx</td>
							  <td>2000</td>
							  <td>1000</td>
							  <td>1000</td>
							  <td>1000</td>
							  <td>1000</td>
							</tr>
							<tr>
							  <td>8</td>
							  <td>xxxxxx</td>
							  <td>2000</td>
							  <td>1000</td>
							  <td>1000</td>
							  <td>1000</td>
							  <td>1000</td>
							</tr>
							<tr>
							  <td>9</td>
							  <td>xxxxxx</td>
							  <td>2000</td>
							  <td>1000</td>
							  <td>1000</td>
							  <td>1000</td>
							  <td>1000</td>
							</tr>
							<tr>
							  <td>10</td>
							  <td>xxxxxx</td>
							  <td>2000</td>
							  <td>1000</td>
							  <td>1000</td>
							  <td>1000</td>
							  <td>1000</td>
							</tr>
						</tbody>
					</table>
				</div>
				<a href="" class="btn-black pull-right">
					Lihat Semua
				</a>
			</div>
			<div class="statistik-dashboard-area kecamatan">
				<h1>Kelurahan / Desa</h1>
				<div class="tbl-header-statistik">
					<table cellpadding="0" cellspacing="0" border="0">
					  <thead>
						<tr>
						  <th>Peringkat</th>
						  <th>Kelurahan/Desa</th>
						  <th>Main</th>
						  <th>Menang</th>
						  <th>Seri</th>
						  <th>Kalah</th>
						  <th>Point</th>
						</tr>
					  </thead>
					</table>
				</div>
				<div class="tbl-content-statistik">
					<table cellpadding="0" cellspacing="0" border="0">
						<tbody>
							<tr>
							  <td>1</td>
							  <td>xxxxxx</td>
							  <td>2000</td>
							  <td>1000</td>
							  <td>1000</td>
							  <td>1000</td>
							  <td>1000</td>
							</tr>
							<tr>
							  <td>2</td>
							  <td>xxxxxx</td>
							  <td>2000</td>
							  <td>1000</td>
							  <td>1000</td>
							  <td>1000</td>
							  <td>1000</td>
							</tr>
							<tr>
							  <td>3</td>
							  <td>xxxxxx</td>
							  <td>2000</td>
							  <td>1000</td>
							  <td>1000</td>
							  <td>1000</td>
							  <td>1000</td>
							</tr>
							<tr>
							  <td>4</td>
							  <td>xxxxxx</td>
							  <td>2000</td>
							  <td>1000</td>
							  <td>1000</td>
							  <td>1000</td>
							  <td>1000</td>
							</tr>
							<tr>
							  <td>5</td>
							  <td>xxxxxx</td>
							  <td>2000</td>
							  <td>1000</td>
							  <td>1000</td>
							  <td>1000</td>
							  <td>1000</td>
							</tr>
						</tbody>
					</table>
				</div>
				<a href="" class="btn-black pull-right">
					Lihat Semua
				</a>
			</div>
			<div class="statistik-dashboard-area kota">
				<h1>Kelurahan / Desa</h1>
				<div class="tbl-header-statistik">
					<table cellpadding="0" cellspacing="0" border="0">
					  <thead>
						<tr>
						  <th>Peringkat</th>
						  <th>Kelurahan/Desa</th>
						  <th>Main</th>
						  <th>Menang</th>
						  <th>Seri</th>
						  <th>Kalah</th>
						  <th>Point</th>
						</tr>
					  </thead>
					</table>
				</div>
				<div class="tbl-content-statistik">
					<table cellpadding="0" cellspacing="0" border="0">
						<tbody>
							<tr>
							  <td>1</td>
							  <td>xxxxxx</td>
							  <td>2000</td>
							  <td>1000</td>
							  <td>1000</td>
							  <td>1000</td>
							  <td>1000</td>
							</tr>
							<tr>
							  <td>2</td>
							  <td>xxxxxx</td>
							  <td>2000</td>
							  <td>1000</td>
							  <td>1000</td>
							  <td>1000</td>
							  <td>1000</td>
							</tr>
							<tr>
							  <td>3</td>
							  <td>xxxxxx</td>
							  <td>2000</td>
							  <td>1000</td>
							  <td>1000</td>
							  <td>1000</td>
							  <td>1000</td>
							</tr>
							<tr>
							  <td>4</td>
							  <td>xxxxxx</td>
							  <td>2000</td>
							  <td>1000</td>
							  <td>1000</td>
							  <td>1000</td>
							  <td>1000</td>
							</tr>
							<tr>
							  <td>5</td>
							  <td>xxxxxx</td>
							  <td>2000</td>
							  <td>1000</td>
							  <td>1000</td>
							  <td>1000</td>
							  <td>1000</td>
							</tr>
							<tr>
						</tbody>
					</table>
				</div>
				<a href="" class="btn-black pull-right">
					Lihat Semua
				</a>
			</div>
			<div class="score-area score-area-statistik">
				<div class="row">
					<div class="col-4">
						<h4>
							5 Point
						</h4>
						<a href="#" class="btn-score">
							Menang
						</a>
					</div>
					<div class="col-4">
						<h4>
							4 Point
						</h4>
						<a href="#" class="btn-score">
							Seri
						</a>
					</div>
					<div class="col-4">
						<h4>
							6 Point
						</h4>
						<a href="#" class="btn-score">
							Kalah
						</a>
					</div>
				</div>
			</div>
		</div>
	</body>
</html>