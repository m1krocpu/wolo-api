<!DOCTYPE HTML>
<html>
	<head>
		<title></title>
		<meta name="viewport" content="width=device-width, user-scalable=no" />
		<link rel="stylesheet" href="{{url('/')}}/assets/css/style.css"/>
		<link rel="stylesheet" href="{{url('/')}}/assets/css/ionicons.min.css"/>
	</head>
	<body>
		<div id="containter-content">
			<div class="box-point pull-left">
				<span class="box-point-desc-count">
					<?php echo ($riwayat?$riwayat['poin']:0) ?>
				</span>
				<p class="box-point-desc">
					Point
				</p>
			</div>
			<div class="info-user" style="padding-top:13px;">
				<h4>
					<span class="icon-user">@</span>{{$d->username}}
				</h4>
				<ul class="info-address">
					<li>Kelurahan : <span>{{$d->desa_nama}}</span></li>
					<li>Kecamatan : <span>{{$d->kecamatan_nama}}</span></li>
					<li>Kabupaten/Kota : <span>{{$d->kota_nama}}</span></li>
				</ul>	
			</div>
			<div class="change-money pull-right">
				<a href="" class="btn-change-money">
					Tukar
				</a>
				<p class="money-desc" style="padding-top:8px">
					Rp 0
				</p>
			</div>
			<div class="score-area">
				<div class="row">
					<div class="col-3">
						<h4>
							<?php echo ($riwayat?$riwayat['match']:0) ?>
						</h4>
						<a href="#" class="btn-score">
							Main
						</a>
					</div>
					<div class="col-3">
						<h4>
							<?php echo ($riwayat?$riwayat['menang']:0) ?>
						</h4>
						<a href="#" class="btn-score">
							Menang
						</a>
					</div>
					<div class="col-3">
						<h4>
							<?php echo ($riwayat?$riwayat['seri']:0) ?>
						</h4>
						<a href="#" class="btn-score">
							Seri
						</a>
					</div>
					<div class="col-3">
						<h4>
							<?php echo ($riwayat?$riwayat['kalah']:0) ?>
						</h4>
						<a href="#" class="btn-score">
							Kalah
						</a>
					</div>
				</div>
			</div>
			<div class="dashboard-area">
				<h1>5 Duel Terakhir</h1>
					<table cellpadding="0" cellspacing="0" border="0">
					  <thead>
						<tr>
						  <th>Status</th>
						  <th>Lawan</th>
						  <th style="width:100px">Kelurahan/Desa</th>
						  <th>Skor</th>
						  <th>Point</th>
						</tr>
					  </thead>
						<tbody>
						<?php
							if ($riwayat){
								$index=0;
								foreach($riwayat['data'] as $r){
						?>
							<tr>
							  <td>{{$r['status']}}</td>
							  <td>{{$r['lawan']}}</td>
							  <td>{{$r['desa']}}</td>
							  <td>{{$r['score']}}</td>
							  <td>{{$r['poin']}}</td>
							</tr>	
						<?php			
									$index++;
									if ($index==5){
										break;
									}
								}
							}
						?>						
						</tbody>
					</table>
			</div>
		<div class="prestasi-area">
			<h4 class="title-prestasi">Prestasi</h4>
			<div class="row">
				<div class="col-6">
					<div class="info-box-prestasi">
						<h4 class="title-info-box">
							Kelurahan <span>{{$d->desa_nama}}</span>
						</h4>
						<div class="info-count pull-left">
							<span>Total Penduduk</span>
							<h4>{{$prestasi['desa']}}</h4>
						</div>
						<div class="info-count pull-right">
							<span>Urutan</span>
							<h4>{{$prestasi['r_desa']}}</h4>
						</div>
					</div>
				</div>
				<div class="col-6">
					<div class="info-box-prestasi">
						<h4 class="title-info-box">
							Kecamatan <span>{{$d->kecamatan_nama}}</span>
						</h4>
						<div class="info-count pull-left">
							<span>Total Penduduk</span>
							<h4>{{$prestasi['kecamatan']}}</h4>
						</div>
						<div class="info-count pull-right">
							<span>Urutan</span>
							<h4>{{$prestasi['r_kecamatan']}}</h4>
						</div>
					</div>
				</div>
				<div class="col-6">
					<div class="info-box-prestasi">
						<h4 class="title-info-box">
							Kabupaten/Kota <span>{{$d->kota_nama}}</span>
						</h4>
						<div class="info-count pull-left">
							<span>Total Penduduk</span>
							<h4>{{$prestasi['kota']}}</h4>
						</div>
						<div class="info-count pull-right">
							<span>Urutan</span>
							<h4>{{$prestasi['r_kota']}}</h4>
						</div>
					</div>
				</div>
				<div class="col-6">
					<div class="info-box-prestasi">
						<h4 class="title-info-box">
							Propinsi <span>Gorontalo</span>
						</h4>
						<div class="info-count pull-left">
							<span>Total Penduduk</span>
							<h4>{{$prestasi['provinsi']}}</h4>
						</div>
						<div class="info-count pull-right">
							<span>Urutan</span>
							<h4>{{$prestasi['r_provinsi']}}</h4>
						</div>
					</div>
				</div>
			</div>
		</div>
	</div>
	</body>
</html>