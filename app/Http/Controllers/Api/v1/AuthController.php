<?php

namespace App\Http\Controllers\Api\v1;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\DB;
use App\User;
use JWTAuth;
use JWTFactory;
use Tymon\JWTAuth\Exceptions\JWTException;
use Illuminate\Support\Facades\Hash;

class AuthController extends Controller
{

	private function setLog($session, $ket){
		return DB::table("wolo_log")->insert(["log_session"=>$session, "log_ket"=>$ket]);
	}

	public function nik(Request $request){
    	$this->validate($request,['nik'=>'required','hp'=>'required']);


    	$check = DB::table("users")
			->where("akun_penduduk",$request["nik"])
			->first();
		if (count($check)>0){
			return response()->json(['success'=>0, 'exception' => 'already_registered'], 200);
		}else{

	     	$result = DB::table("wolo_penduduk")
				 ->where("penduduk_nik",$request["nik"])
				 ->first();
			if (count($result)<=0){
				$result = $this->check_nik_kpu("https://pilkada2017.kpu.go.id", $request["nik"]);
			}
			if (count($result)<=0){
				$result = $this->check_nik_kpu("https://infopemilu.kpu.go.id/pilkada2018", $request["nik"]);
			}

			if (count($result)>0){
				$nomor = $request['hp'];
				$kode = rand(100000,999999);

				if (DB::table("wolo_penduduk")->where("penduduk_nik",$request['nik'])->update(["penduduk_verifikasi"=>$kode, "penduduk_hp"=>$nomor])){
					$this->sms($nomor,$kode);
				}

				return response()->json(['success'=>1, 'exception' => ''], 200);
			}
		}
		return response()->json(['success'=>0, 'exception' => 'not_found'], 200);
	}

	public function registernik(Request $request){
    	$this->validate($request,['nik'=>'required','hp'=>'required','nama'=>'required','desa'=>'required']);


    	$check = DB::table("wolo_penduduk")
			->where("penduduk_nik",$request["nik"])
			->first();
		if (count($check)>0){
			return response()->json(['success'=>0, 'exception' => 'already_registered'], 200);
		}else{
			$kode = rand(100000,999999);

			$daftar['penduduk_nik'] = $request['nik'];
			$daftar['penduduk_nama'] = $request['nama'];
			$daftar['penduduk_desa'] = $request['desa'];
			$daftar['penduduk_hp'] = $request['hp'];
			$daftar['penduduk_valid'] = "0";
			$daftar['penduduk_verifikasi'] = $kode;

			if (DB::table("wolo_penduduk")->insert($daftar)){
				$this->sms($request['hp'],$kode);
			}
		}
		return response()->json(['success'=>1, 'exception' => ''], 200);
	}



	private function check_nik_kpu($url, $nik){
		$url = $url."/pemilih/dps/1/hasil-cari/resultDps.json?nik=".$nik."&nama=&namaPropinsi=GORONTALO&namaKabKota=&namaKecamatan=&namaKelurahan=&notificationType=&_=1525346058337";
		$curlHandle = curl_init();
		curl_setopt($curlHandle, CURLOPT_URL, $url);
		curl_setopt($curlHandle, CURLOPT_HEADER, 0);
		curl_setopt($curlHandle, CURLOPT_RETURNTRANSFER, 1);
		curl_setopt($curlHandle, CURLOPT_SSL_VERIFYHOST, 2);
		curl_setopt($curlHandle, CURLOPT_SSL_VERIFYPEER, 0);
		curl_setopt($curlHandle, CURLOPT_TIMEOUT,30);
		$results = curl_exec($curlHandle);
		curl_close($curlHandle);

		$data = json_decode($results,true);
		if (count($data['aaData'])>0){
			$penduduk = $data['aaData'][0];
			$result = DB::table("wolo_penduduk")
				 ->where("penduduk_nik",$nik)
				 ->first();
			if (count($result)<=0){
				$desa=0;
				$dbDesa = DB::table("wolo_desa")
				 ->where("desa_nama",$penduduk["namaKelurahan"])
				 ->first();
				 if (count($dbDesa)>0){
				 	$desa = $dbDesa->desa_id;
				 }

				$d['penduduk_nik'] = $nik;
				$d['penduduk_nama'] = $penduduk['nama'];
				$d['penduduk_desa'] = $desa;
				DB::table("wolo_penduduk")->insert($d);
			}
			return $penduduk;
		}		
		$this->setLog("NIK_NOT_FOUND",$nik);
		return array();
	}

	private function sms($telepon, $kode){
		$userkey = "dt02ws";
		$passkey = "1sampai8";
		$message = "WOLO-".$kode;
		$url = "https://reguler.zenziva.net/apps/smsapi.php";
		$curlHandle = curl_init();
		curl_setopt($curlHandle, CURLOPT_URL, $url);
		curl_setopt($curlHandle, CURLOPT_POSTFIELDS, 'userkey='.$userkey.'&passkey='.$passkey.'&nohp='.$telepon.'&pesan='.urlencode($message));
		curl_setopt($curlHandle, CURLOPT_HEADER, 0);
		curl_setopt($curlHandle, CURLOPT_RETURNTRANSFER, 1);
		curl_setopt($curlHandle, CURLOPT_SSL_VERIFYHOST, 2);
		curl_setopt($curlHandle, CURLOPT_SSL_VERIFYPEER, 0);
		curl_setopt($curlHandle, CURLOPT_TIMEOUT,30);
		curl_setopt($curlHandle, CURLOPT_POST, 1);
		$results = curl_exec($curlHandle);
		curl_close($curlHandle);

		$XMLdata = new \SimpleXMLElement($results);
		$status = $XMLdata->message[0]->text;
		$this->setLog("SMSGATEWAY",$status);
		//echo $status;
	}

	public function register(Request $request){
    	$this->validate($request,['nik'=>'required','username'=>'required','password'=>'required','kode'=>'required']);
    	$result = DB::table("wolo_penduduk")
			->where("penduduk_nik",$request["nik"])
			->where("penduduk_verifikasi",$request["kode"])
			->first();

		if (count($result)>0){
			$check = DB::table("users")->where("username",$request['username'])->get();
			if (count($check)>0){
				return response()->json(['success'=>0, 'exception' => 'username_exists'], 200);
			}else{
				$user['akun_penduduk'] = $request['nik'];
				$user['username'] = $request['username'];
				$user['password'] = Hash::make($request['password']);
				$user['client_id'] = "wolo-android";
				$user['client_secret'] = substr(base64_encode(md5($request['username'].$request['nik'])),0,12);

				$create_user = DB::table("users")->insert($user);
				if ($create_user){
					return $this->authentication($request);
				}
			}
		}
		return response()->json(['success'=>0, 'exception' => 'invalid_code'], 200);
	}
	
	private function setToken($client_id,$client_secret,$refresh_token){
		return DB::table("users")->where("client_id",$client_id)->where("client_secret",$client_secret)->update(["_token"=>$refresh_token]);
	}
	
    public function authentication(Request $request){
    	$this->validate($request,['username'=>'required','password'=>'required']);
    	 // grab credentials from the request
		 
		$result = DB::table("users")
			->join("wolo_penduduk","penduduk_nik","akun_penduduk")
			->select("id","username","akun_penduduk","client_id", "client_secret", "client_scope")
			->whereNotNull("penduduk_hp")
			->whereRaw("(username='".$request["username"]."' OR akun_penduduk='".$request["username"]."' OR penduduk_hp='".$request["username"]."')")
			->first();
		
		$auth = array();
		if (count($result)>0){
			$auth = $request->only('username', 'password');
			$auth['username'] = $result->username;
		}else{
            return response()->json(['exception' => 'invalid_credentials'], 200);
		}		
		
        try {
            // attempt to verify the credentials and create a token for the user
            
            if (! $token = JWTAuth::attempt($auth)) {
                return response()->json(['exception' => 'invalid_credentials'], 200);
            }
			$token = JWTAuth::fromUser($result);
        } catch (JWTException $e) {
            // something went wrong whilst attempting to encode the token
            return response()->json(['exception' => 'could_not_create_token'], 200);
        }

		$customClaims = json_decode(json_encode($result),true);
		$payload = JWTFactory::make($customClaims);
		$refresh_token = md5(JWTAuth::encode($payload)->get()).md5($result->client_id);
		if ($this->setToken($result->client_id,$result->client_secret,$refresh_token)){
			$user = array();
			$userPrepare = DB::table("wolo_penduduk")
						->join("wolo_desa","desa_id","penduduk_desa")
						->join("wolo_kecamatan","kecamatan_id","desa_kecamatan")
						->join("wolo_kota","kota_id","kecamatan_kota")
						->where("penduduk_nik",$result->akun_penduduk)
						->where("penduduk_status",1)
						->first();
			if (count($userPrepare)>0){
				$user['nama'] = $auth["username"];
				$user['nik'] = $userPrepare->penduduk_nik;
				$user['desa'] = $userPrepare->penduduk_desa;
				$user['desa_nama'] = $userPrepare->desa_nama;
				$user['kecamatan_nama'] = $userPrepare->kecamatan_nama;
				$user['kota_nama'] = $userPrepare->kota_nama;
				$user['client_id'] = $result->client_id;
				$user['client_secret'] = $result->client_secret;
				$data["status"] = "1";
				$data["token_type"] = "Bearer";
			}else{
				$data['status']="0";
			}
			$data["refresh_token"] = $refresh_token;
			$data["access_token"] = $token;
			$data["scope"] = $result->client_scope;
			$data["tanggal"] = date("Y-m-d");
			$data["data"] = $user;
			$data["exception"] = "";
			return response()->json($data);
			

		}
            return response()->json(['exception' => 'could_not_create_token'], 200);	
		
    }	
	
	public function refresh(Request $request){
		$this->validate($request,['grant_type'=>'required','client_id'=>'required','client_secret'=>'required','code'=>'required']);
		
		if($request['grant_type'] != "refresh_token"){
            return response()->json(['exception' => 'grant_type not found'], 200);
		}
		
		$result = DB::table("users")
			->select("id","akun_penduduk","client_id", "client_secret", "client_scope")
			->where("client_id", $request["client_id"])
			->where("client_secret", $request["client_secret"])
			//->where("_token", $request["code"])
			->first();
		if (count($result)<=0){			
			return response()->json(['error' => 'invalid_credentials'], 200);
		}
		
		try {
            
			if (!$token = JWTAuth::fromUser($result)){
				return response()->json(['exception' => 'invalid_credentials'], 200);
			}
        } catch (JWTException $e) {
            // something went wrong whilst attempting to encode the token
            return response()->json(['exception' => 'could_not_create_token'], 200);
        }
		$userPrepare = DB::table("wolo_penduduk")
						->join("wolo_desa","desa_id","penduduk_desa")
						->join("wolo_kecamatan","kecamatan_id","desa_kecamatan")
						->join("wolo_kota","kota_id","kecamatan_kota")
						->where("penduduk_nik",$result->akun_penduduk)
						->where("penduduk_status",1)
						->first();
			if (count($userPrepare)>0){
				$data["status"] = "1";
			}else{
				$data["status"] = "0";
			}

		
		$customClaims = json_decode(json_encode($result),true);
		$payload = JWTFactory::make($customClaims);
		$refresh_token = md5(JWTAuth::encode($payload)->get()).md5($result->client_id);
		if ($this->setToken($result->client_id,$result->client_secret,$refresh_token)){
			$data["refresh_token"] = $refresh_token;
			$data["token_type"] = "Bearer";
			$data["tanggal"] = date("Y-m-d");
			$data["access_token"] = $token;
			$data["scope"] = $result->client_scope;
			$data["exception"] = "";
			return response()->json($data);
		}else{
            return response()->json(['exception' => 'could_not_create_token'], 200);	
		}
	}
}
