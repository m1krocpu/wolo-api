<?php

namespace App\Http\Controllers;

use Request;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\DB;
use Illuminate\Http\File;
use Illuminate\Support\Facades\Storage;
use Illuminate\Support\Facades\Hash;
use Illuminate\Routing\UrlGenerator;
use DateTime;

class ApiMobileController extends Controller
{
	
	private function auth($uid){
		$auth = DB::table("users")
			->join("wolo_penduduk","penduduk_nik","akun_penduduk")
			->join("wolo_desa","desa_id","penduduk_desa")
			->join("wolo_kecamatan","kecamatan_id","desa_kecamatan")
			->join("wolo_kota","kota_id","kecamatan_kota")
			->select("username","desa_nama")->where("client_secret",$uid)->first();
		if (count($auth)==1){
			return $auth;
		}	
		return false;
	}
	
	private function StringNull($item){
		$data = array();
		foreach ($item as $key => $value) {
			if ($value != ""){
				$data[$key] = $value;
			}else{
				$data[$key] = null;
			}
		}
		return $data;
	}

    public function desa(){
    	$data = DB::table("wolo_desa")->get();

    	
        echo json_encode($data);
	return "";
    }
	
	
    public function kuis(){
    	$data = array();
		$uid = Request::get("uid");
    	if ($uid!=null){
	    	if ($this->auth($uid)){
	    		$req = Request::get("id");
	    		$now = date("H:i:s");
	    		$data = DB::table('wolo_kuis')
		    		->select(DB::raw("kuis_isi as kuis"))
		    		->where("kuis_id",$req)
		    		->whereRaw("kuis_mulai <= CAST('$now' as time) AND kuis_selesai >= CAST('$now' as time)")
		            ->first();
			
			if (count($data)>0){
				require_once 'Lib/firebaseInterface.php';
				require_once 'Lib/firebaseLib.php';
				require_once 'Lib/firebaseStub.php'; 
				$firebase = new \Firebase\FirebaseLib("https://wolo-a0a9c.firebaseio.com/",''); 
		 
				$firebase->set("kuis/soal/".$req, $data->kuis);
			}

	    	}
	    }

        echo json_encode($data);
	return "";
    }

    public function setKuis(){
    	$result['success'] = "0";	
		$uid = Request::get("uid");
	    if ($uid!=null){
	    	$user = $this->auth($uid);
	    	if ($user && isset($_POST['data']) && isset($_POST['id'])){
	    		$id = Request::get("id");
	    		$data = Request::get("data");
	    		$now = date("H:i:s");
			
			$akun = DB::table("users")->where("username",$user->username)->first();

	    		$check = DB::table('wolo_kuis')
		    		->select(DB::raw("kuis_isi as kuis"))
		    		->where("kuis_id",$id)
		    		->whereRaw("kuis_mulai <= CAST('$now' as time) AND kuis_selesai >= CAST('$now' as time)")
		            ->first();

		        if (count($check)>0){
		    		$chat['kjawab_akun'] = $user->username;
		    		$chat['kjawab_isi'] = $data;
		    		$chat['kjawab_kuis'] = $id;

		    		$insertId = DB::table("wolo_kuis_jawab")->insertGetId($chat);
		    		$proses = DB::table('wolo_kuis_jawab')->where("kjawab_id",$insertId)->first();

		    			$dataFb['usr'] = $proses->kjawab_akun;
		    			$dataFb['msg'] = $proses->kjawab_isi;
		    			$dataFb['time'] = $now;
		    			$dataFb['scope'] = $akun->client_scope;

						require_once 'Lib/firebaseInterface.php';
						require_once 'Lib/firebaseLib.php';
						require_once 'Lib/firebaseStub.php'; 
						$firebase = new \Firebase\FirebaseLib("https://wolo-a0a9c.firebaseio.com/",''); 
		 
						$firebase->push("kuis/jawab/".$id, $dataFb);
		    		if (count($proses)>0){
		    		
	    				$result['success'] = 1;
		    		}
		        }
	    	}
		}

        echo json_encode($result);
	return "";
    }


    public function Obrolan(){
    	$result['success'] = "0";	
	$uid = Request::get("uid");
	if ($uid!=null){
	    	$user = $this->auth($uid);
	    	if ($user && isset($_POST['data']) && isset($_POST['state'])){
	    		$state = Request::get("state");
	    		$data = Request::get("data");
			$akun = DB::table("users")->where("username",$user->username)->first();

	    		$dataFb['usr'] = $user->username;
		    	$dataFb['msg'] = $data;
		    	$dataFb['scope'] = $akun->client_scope;
		    	$dataFb['time'] = date("Y-m-d H:i:s");

			require_once 'Lib/firebaseInterface.php';
			require_once 'Lib/firebaseLib.php';
			require_once 'Lib/firebaseStub.php'; 
			$firebase = new \Firebase\FirebaseLib("https://wolo-a0a9c.firebaseio.com/",'');
			$firebase->push("obrolan/".$state, $dataFb);
			$last = str_replace("\n"," ",$data);
			if (strlen($last)>22){
				$last = substr($last,0,22)."...";
			}
			$firebase->set("obrolan/last/".$state, $last);

			$result['success'] = "1";	    	
		}
	}

        echo json_encode($result);
	return "";
    }

    public function kuisWinner(){
    	$result['winner'] = "";	
		$uid = Request::get("uid");
	    if ($uid!=null){
	    	if ($this->auth($uid) && isset($_POST['id'])){
	    		$id = Request::get("id");
	    		$now = date("H:i:s");

	    		$check = DB::table('wolo_kuis')
		    		->where("kuis_id",$id)
		    		->whereRaw("kuis_selesai <= CAST('$now' as time)")
		            ->first();

		        if (count($check)>0){
		    		$winner = DB::table("wolo_kuis_jawab")
		    					->join("users","username","kjawab_akun")
		    					->join("wolo_penduduk","penduduk_nik","akun_penduduk")
		    					->join("wolo_desa","desa_id","penduduk_desa")
		    					->whereRaw("LOWER(kjawab_isi) like LOWER('".$check->kuis_jawab."')")
		    					->orderBy("kjawab_created","ASC")
		    					->first();

		    		if (count($winner)>0){
		    			$result['winner'] = $check->kuis_isi." ".$check->kuis_jawab."\n\n"."Selamat kepada ".$winner->penduduk_nama." (".$winner->username.") \n"."Dari ".$winner->desa_nama;
		    		}else{
		    			$result['winner'] = $check->kuis_isi." ".$check->kuis_jawab."\n\n"."Tidak Ada pemenang pada KUIS ini.";
		    		}

				require_once 'Lib/firebaseInterface.php';
				require_once 'Lib/firebaseLib.php';
				require_once 'Lib/firebaseStub.php'; 
				$firebase = new \Firebase\FirebaseLib("https://wolo-a0a9c.firebaseio.com/",''); 
		 
				$firebase->set("kuis/winner/".$id, $result['winner']);
			
		        }
			}
		}

        echo json_encode($result);
	return "";
    }


    public function duel(){
    	$soal = array();
    	$soal['soal'] = "";
    	$soal['status'] = "";
		$uid = Request::get("uid");
    	if ($uid!=null){
    		$user = $this->auth($uid);
	    	if ($user && isset($_POST['id']) && isset($_POST['k'])){
	    		$id = Request::get("id");
	    		$kategori = Request::get("k");
	    		$check = DB::table("wolo_duel")
						->join("users","duel_akun","username")
						->join("wolo_penduduk","penduduk_nik","akun_penduduk")
						->join("wolo_desa","desa_id","penduduk_desa")
						->whereNotNull("duel_enemy")
	    				->where("duel_id",$id)->first();

	    		if (count($check)>0){
	    			$t = DB::table("wolo_duel_soal")
	    						->join("wolo_soal","soal_id","dsoal_soal")
	    						->leftjoin("wolo_duel_jawab","djawab_soal","soal_id")
	    						->where("dsoal_duel",$id)
	    						->where("soal_kategori",$kategori)
	    						->where("dsoal_akun",$user->username)
	    						->first();

	    			if (count($t)>0){
					    $soal['uid'] = $t->soal_id;
					    $soal['soal'] = $t->soal_isi;
					    $soal['num'] = strlen($t->soal_jawab);
					    $soal['space'] = !strpos($t->soal_jawab, " ")?0:(strpos($t->soal_jawab, " ")+1);
					    if ($t->djawab_soal!=""){
					    	$soal['status'] = "answered";
					    }
	    			}else{
			    		$data = DB::table('wolo_soal')
	    					->leftjoin("wolo_duel_jawab","djawab_soal","soal_id")
				    		->where("soal_kategori",$kategori)
				    		->where("soal_status",1)
				    		//->whereRaw(" NOT EXISTS (SELECT * FROM wolo_duel_soal WHERE wolo_duel_soal.dsoal_soal = soal_id)")
				            ->get();
					/* if (count($data)<=0){
					     $reset = DB::table("wolo_duel_soal")->where("dsoal_akun",$check->duel_akun)->delete();
					     $data = DB::table('wolo_soal')
	    					->leftjoin("wolo_duel_jawab","djawab_soal","soal_id")
				    		->where("soal_kategori",$kategori)
				    		->whereRaw(" NOT EXISTS (SELECT * FROM wolo_duel_soal WHERE wolo_duel_soal.dsoal_soal = soal_id)")
				            ->get();

					} */
				        if (count($data)>0){
					        $random_index = rand(0, count($data)-1);
					        $t = $data[$random_index];
					        $history['dsoal_duel'] = $id;
					        $history['dsoal_soal'] = $t->soal_id;
					        $history['dsoal_akun'] = $check->duel_akun;
					        DB::table('wolo_duel_soal')->insert($history);
					        $history['dsoal_akun'] = $check->duel_enemy;
					        DB::table('wolo_duel_soal')->insert($history);

					    	$soal['uid'] = $t->soal_id;
					        $soal['soal'] = $t->soal_isi;
					        $soal['num'] = strlen($t->soal_jawab);
					        $soal['space'] = !strpos($t->soal_jawab, " ")?0:(strpos($t->soal_jawab, " ")+1);
					        if ($t->djawab_soal!=""){
					    	$soal['status'] = "answered";
					    }
					}
	    			}
	    		}
	    	}
	    }

        echo json_encode($soal);
	return "";
    }

    public function setDuel(){
    	$result['success'] = "0";	
    	$result['room'] = "";	
		$uid = Request::get("uid");
	    if ($uid!=null){
	    	$user = $this->auth($uid);
	    	if ($user){
			require_once 'Lib/firebaseInterface.php';
			require_once 'Lib/firebaseLib.php';
			require_once 'Lib/firebaseStub.php';
 			$firebase = new \Firebase\FirebaseLib("https://wolo-a0a9c.firebaseio.com/",''); 

	    			$duel['duel_akun'] = $user->username;
				$last = DB::table("wolo_duel")->where("duel_akun",$user->username)->whereNull("duel_enemy")->get();
				foreach($last as $l){
					DB::table("wolo_duel")->where("duel_id",$l->duel_id)->delete();
					$firebase->delete("duel/room/".$l->duel_id);
				}
	    			$insertId = DB::table("wolo_duel")->insertGetId($duel);

				if ($insertId){
		    			$dataFb['p1'] = $user->username;
		    			$dataFb['d1'] = $user->desa_nama;

					$firebase->set("duel/room/".$insertId, $dataFb);
		    		
	    				$result['room'] = $insertId;
	    				$result['success'] = 1;
		    		}
	    	}
	    }

        echo json_encode($result);
	return "";
	}

    public function joinDuel(){
    	$result['success'] = "0";	
		$uid = Request::get("uid");
	    if ($uid!=null){
	    	$user = $this->auth($uid);
	    	if ($user && isset($_POST['room'])){
	    		$room = Request::get("room");
	    		$check = DB::table("wolo_duel")
						->join("users","duel_akun","username")
						->join("wolo_penduduk","penduduk_nik","akun_penduduk")
						->join("wolo_desa","desa_id","penduduk_desa")
						//->whereNull("duel_enemy")
	    				->where("duel_id",$room)->first();

	    		if ($check){
	    			$duel['duel_enemy'] = $user->username;
	    			$update = DB::table("wolo_duel")->where("duel_id",$room)->whereNull("duel_enemy")->update($duel);

					if ($update){
		    			$dataFb['p1'] = $check->duel_akun;
		    			$dataFb['d1'] = $check->desa_nama;
		    			$dataFb['p2'] = $user->username;
		    			$dataFb['d2'] = $user->desa_nama;
		    			$dataFb['start'] = $check->duel_updated;

						require_once 'Lib/firebaseInterface.php';
						require_once 'Lib/firebaseLib.php';
						require_once 'Lib/firebaseStub.php'; 
						$firebase = new \Firebase\FirebaseLib("https://wolo-a0a9c.firebaseio.com/",''); 

						$soal = array();
						for($i=1;$i<6;$i++){
							$soal[$i] = $this->generateSoal($room, $dataFb, $i);
						}
		 
						$firebase->set("duel/room/".$room, null);
						$firebase->set("duel/soal/".$room, $soal);
						$firebase->set("duel/run/".$room, $dataFb);
		    		
	    				$result['success'] = 1;
		    		}
		    	}
	    	}
	    }

        echo json_encode($result);
	return "";
	}

	private function generateSoal($id, $duel, $kategori){
		$soal=array();
		$data = DB::table('wolo_soal')
	    		->leftjoin("wolo_duel_jawab","djawab_soal","soal_id")
			->where("soal_kategori",$kategori)
			->where("soal_status",1)
			->whereRaw(" NOT EXISTS (SELECT * FROM wolo_duel_soal WHERE wolo_duel_soal.dsoal_soal = soal_id AND (dsoal_akun='".$duel['p1']."' OR dsoal_akun='".$duel['p2']."'))")
			->get();
		if (count($data)<=0){
			$data = DB::table('wolo_soal')
	    			->leftjoin("wolo_duel_jawab","djawab_soal","soal_id")
				->where("soal_kategori",$kategori)
				->whereRaw(" NOT EXISTS (SELECT * FROM wolo_duel_soal WHERE wolo_duel_soal.dsoal_soal = soal_id AND dsoal_akun='".$duel['p1']."')")	
				->where("soal_status",1)
				->get();
			if (count($data)<=0){
				$data = DB::table('wolo_soal')
	    				->leftjoin("wolo_duel_jawab","djawab_soal","soal_id")
					->where("soal_kategori",$kategori)
					->whereRaw(" NOT EXISTS (SELECT * FROM wolo_duel_soal WHERE wolo_duel_soal.dsoal_soal = soal_id AND dsoal_akun='".$duel['p2']."')")	
					->where("soal_status",1)
					->get();
				if (count($data)<=0){
					$data = DB::table('wolo_soal')
	    					->leftjoin("wolo_duel_jawab","djawab_soal","soal_id")
						->where("soal_kategori",$kategori)
						->where("soal_status",1)
						->get();

				}

			}
		}
			$random_index = rand(0, count($data)-1);
			$t = $data[$random_index];
			$history['dsoal_duel'] = $id;
			$history['dsoal_soal'] = $t->soal_id;
			$history['dsoal_akun'] = $duel['p1'];
			DB::table('wolo_duel_soal')->insert($history);
			$history['dsoal_akun'] = $duel['p2'];
			DB::table('wolo_duel_soal')->insert($history);

			$soal['uid'] = $t->soal_id;
			$soal['soal'] = $t->soal_isi;
			$soal['num'] = strlen($t->soal_jawab);
			$soal['space'] = !strpos($t->soal_jawab, " ")?0:(strpos($t->soal_jawab, " ")+1);
		
		return $soal;
	}

    	public function jawabDuel(){
    		$result['success'] = "0";
		$uid = Request::get("uid");
	    if ($uid!=null){
	    	$user = $this->auth($uid);
	    	if ($user && isset($_POST['id']) && isset($_POST['jawab']) && isset($_POST['soal'])){
	    		$id = Request::get("id");
	    		$soal = Request::get("soal");
	    		$jawab = strtolower(Request::get("jawab"));

	    		$check = DB::table("wolo_duel_soal")
	    					->join("wolo_soal","soal_id","dsoal_soal")
	    					->where("dsoal_duel",$id)
	    					->where("dsoal_soal",$soal)
	    					->where("dsoal_akun",$user->username)
	    					->first();
							
	    		if (count($check)>0){
				$jawaban['djawab_status']=0;
				if (trim(strtolower($check->soal_jawab)) == trim(strtolower($jawab))){
					$jawaban['djawab_status']=1;
		    			$result['success'] = "1";
				}

	    			$jawaban['djawab_akun'] = $user->username;
	    			$jawaban['djawab_isi'] = $jawab;
	    			$jawaban['djawab_duel'] = $id;
	    			$jawaban['djawab_soal'] = $soal;		    			
	    			if (!DB::table("wolo_duel_jawab")->insert($jawaban)){
					$log['log_session']="JAWAB DUEL";
					$log['log_akun']=$user->username;
					$log['log_ket'] = json_encode($jawaban);
					DB::table("wolo_log")->insert($log);
				}

		    		$score = DB::table("wolo_duel_jawab")
		    					->join("wolo_soal","soal_id","djawab_soal")
		    					->where("djawab_duel", $id)
		    					->where("djawab_akun",$user->username)
		    					->whereRaw("lower(djawab_isi) like lower(soal_jawab)")
		    					->get();
		    		if (count($score)>0){
					require_once 'Lib/firebaseInterface.php';
					require_once 'Lib/firebaseLib.php';
					require_once 'Lib/firebaseStub.php'; 
					$firebase = new \Firebase\FirebaseLib("https://wolo-a0a9c.firebaseio.com/",''); 
				 
					$firebase->set("duel/run/".$id."/".$user->username, count($score));

		    		}
	    		}	    		
	    	}
	    }

        	echo json_encode($result);
		return "";
	}

	public function profile(){
		$uid = Request::get("uid");
	    if ($uid!=null){
	    	$user = $this->auth($uid);
	    	if ($user){

	    		$d = DB::table("users")
	    				->join("wolo_penduduk","penduduk_nik","akun_penduduk")
	    				->join("wolo_desa","desa_id","penduduk_desa")
	    				->join("wolo_kecamatan","kecamatan_id","desa_kecamatan")
	    				->join("wolo_kota","kota_id","kecamatan_kota")
	    				->where("username", $user->username)
	    				->first();

	    		if (count($d)>0){
	    			$riwayat = $this->getRiwayat($user->username);
	    			$prestasi = $this->getPrestasi($d);
	    			$reward   = $this->getReward($user->username);
				//if ($user->username == "mrgobel"){
				//	return view('prank');
				//}
      				return view('profile',compact("d","riwayat","prestasi","reward"));
      			}
	    	}
	    }
      	return "Silahkan refresh!"; 
	}

	private function getReward($username){
		$reward = 0;
		$kuis = DB::table("wolo_kuis")->get();
		foreach($kuis as $k){
			$data = DB::table("wolo_kuis_jawab")
				->join("wolo_kuis","kuis_id","kjawab_kuis")
		    		->join("users","username","kjawab_akun")
				->where("kjawab_kuis",$k->kuis_id)
		    		->whereRaw("LOWER(kjawab_isi) like LOWER(kuis_jawab)")
		    		->orderBy("kjawab_created","ASC")
		    		->first();
			if (count($data)>0){
				if ($data->username == $username){
					$reward+=$data->kuis_reward;
				}
			}
		}
		return $reward;
	}

	private function getRiwayat($username){
		$stat = DB::table("vw_statistik")
					->whereRaw("(duel_akun like '$username' or duel_enemy like '$username')")
					->orderBy("duel_updated","DESC")
					->take(5)
					->get();
					
		if (count($stat)>0){
			$data['data'] = array();
			$data['menang'] = 0;
			$data['kalah'] = 0;
			$data['seri'] = 0;
			$data['poin'] = 0;
			$data['match'] = count($stat);
			foreach($stat as $a){
				$item = array();
				if ($a->duel_akun == $username){
					$item['lawan'] = $a->duel_enemy;
					$scoreA = $a->score_akun;
					$scoreB = $a->score_enemy;
				}else{
					$item['lawan'] = $a->duel_akun;
					$scoreB = $a->score_akun;
					$scoreA = $a->score_enemy;
				}
				$item['desa'] = $this->getDesa($item['lawan']);
				$item['score'] = $scoreA." - ".$scoreB;
				if ($scoreA > $scoreB){
					$item['poin'] = 3;
					$item['status'] = "MENANG";
					$data['menang']++;
				} else if ($scoreB > $scoreA){
					$item['poin'] = 0;
					$item['status'] = "KALAH";
					$data['kalah']++;
				} else {
					$item['poin'] = 1;
					$item['status'] = "SERI";
					$data['seri']++;
				}
				$data['poin']+=$item['poin'];
				array_push($data['data'],$item);
			}
			return $data;
		}

		return FALSE;
	}

	private function getDesa($username){
		$data = DB::table("users")
	    	->join("wolo_penduduk","penduduk_nik","akun_penduduk")
	    	->join("wolo_desa","desa_id","penduduk_desa")
	    	->where("username",$username)
	    	->first();
	    if (count($data)>0){
	    	return $data->desa_nama;
	    }
	    return "";
	}

	private function getScore($id, $username){
		$data = DB::table("wolo_duel_jawab")->where("djawab_duel",$id)->where("djawab_akun",$username)
				->join("wolo_soal","soal_id","djawab_soal")
				->whereRaw("soal_jawab like djawab_isi")
				->get();
		return count($data);
	}

	private function getPrestasi($user){
		$data['desa'] = count(DB::table("wolo_penduduk")->join("wolo_desa","desa_id","penduduk_desa")->where("desa_id",$user->desa_id)->get());
		$data['kecamatan'] = count(DB::table("wolo_penduduk")->join("wolo_desa","desa_id","penduduk_desa")->join("wolo_kecamatan","kecamatan_id","desa_kecamatan")->where("kecamatan_id",$user->kecamatan_id)->get());
		$data['kota'] = count(DB::table("wolo_penduduk")->join("wolo_desa","desa_id","penduduk_desa")->join("wolo_kecamatan","kecamatan_id","desa_kecamatan")->join("wolo_kota","kota_id","kecamatan_kota")->where("kota_id",$user->kota_id)->get());
		$data['provinsi'] = count(DB::table("wolo_penduduk")->join("wolo_desa","desa_id","penduduk_desa")->join("wolo_kecamatan","kecamatan_id","desa_kecamatan")->join("wolo_kota","kota_id","kecamatan_kota")->get());

		$data['r_desa']="-";
		$data['r_kecamatan']="-";
		$data['r_kota']="-";
		$data['r_provinsi']="-";
		$rank = $this->getRank($user);
		//$rank = false;
		if ($rank){
			$data['r_desa'] = array_search($user->username, array_column($rank['desa'], 'user'))+1;
			$data['r_kecamatan'] = array_search($user->username, array_column($rank['kecamatan'], 'user'))+1;
			$data['r_kota'] = array_search($user->username, array_column($rank['kota'], 'user'))+1;
			$data['r_provinsi'] = array_search($user->username, array_column($rank['provinsi'], 'user'))+1;
		}


		return $data;
	}

	private function getRank($user){
		$data['desa'] = array();
		$data['kecamatan'] = array();
		$data['kota'] = array();
		$data['provinsi'] = array();
		$users = DB::table("users")->join("wolo_penduduk","penduduk_nik","akun_penduduk")
				->join("wolo_desa","desa_id","penduduk_desa")
				->join("wolo_kecamatan","kecamatan_id","desa_kecamatan")
				->join("wolo_kota","kota_id","kecamatan_kota")
				->get();
				
				
		$stat = DB::table("vw_statistik")->get();
		$scores = array();
		if (count($stat)>0){
			foreach($stat as $s){
				if (isset($scores[$s->duel_akun])){
					$scores[$s->duel_akun]+=$s->poin_akun;
				}else{
					$scores[$s->duel_akun]=$s->poin_akun;
				}
				
				
				if (isset($scores[$s->duel_enemy])){
					$scores[$s->duel_enemy]+=$s->duel_enemy;
				}else{
					$scores[$s->duel_enemy]=$s->duel_enemy;
				}
			}
		}
		foreach($users as $u){
			$username = $u->username;

			$item = array();
			$item['user'] = $username;
			$item['desa'] = $u->desa_id;
			$item['poin'] = 0;
			
			if (isset($scores[$username])){
				$item['poin'] = $scores[$username];
			}
			
			if ($u->desa_id == $user->desa_id){
				array_push($data['desa'],$item);
			}
			if ($u->kecamatan_id == $user->kecamatan_id){
				array_push($data['kecamatan'],$item);
			}
			if ($u->kota_id == $user->kota_id){
				array_push($data['kota'],$item);
			}
			array_push($data['provinsi'],$item);
		}

		uasort($data['desa'], function($a, $b) {
		    if ($a['poin'] == $b['poin']) {
		        return 0;
		    }
		    return ($a['poin'] < $b['poin']) ? 1 : -1;
		});

		uasort($data['kecamatan'], function($a, $b) {
		    if ($a['poin'] == $b['poin']) {
		        return 0;
		    }
		    return ($a['poin'] < $b['poin']) ? 1 : -1;
		});

		uasort($data['kota'], function($a, $b) {
		    if ($a['poin'] == $b['poin']) {
		        return 0;
		    }
		    return ($a['poin'] < $b['poin']) ? 1 : -1;
		});

		uasort($data['provinsi'], function($a, $b) {
		    if ($a['poin'] == $b['poin']) {
		        return 0;
		    }
		    return ($a['poin'] < $b['poin']) ? 1 : -1;
		});
		
		return $data;
	}


	public function statistik(){
		$result['kelurahan'] = array();
		$result['kecamatan'] = array();
		$result['kota'] = array();
		$result['mkelurahan'] = array();
		$result['mkecamatan'] = array();
		$result['mkota'] = array();
		$uid = Request::get("uid");
	    if ($uid!=null){
	    	$user = $this->auth($uid);
	    	if ($user){
	    		
				$d = DB::table("users")->join("wolo_penduduk","penduduk_nik","akun_penduduk")
	    				->join("wolo_desa","desa_id","penduduk_desa")
	    				->join("wolo_kecamatan","kecamatan_id","desa_kecamatan")
	    				->join("wolo_kota","kota_id","kecamatan_kota")
	    				->where("username", $user->username)
	    				->first();

				$stat['kelurahan'] = DB::table("wolo_statistik")->where("kategori","kelurahan")->orderBy("rank","ASC")->select("nama","menang","kalah","seri","poin","rank")->get();
				$stat['kecamatan'] = DB::table("wolo_statistik")->where("kategori","kecamatan")->orderBy("rank","ASC")->select("nama","menang","kalah","seri","poin","rank")->get();
				$stat['kota'] = DB::table("wolo_statistik")->where("kategori","kota")->orderBy("rank","ASC")->select("nama","menang","kalah","seri","poin","rank")->get();


      			$i=0;
      			foreach ($stat['kelurahan'] as $k){
      				if ($k->nama==$d->desa_nama){
      					$result['mkelurahan'] = $k;
      				}			
      				if ($i<100){
      					array_push($result['kelurahan'],$k);
      				}
      				$i++;
      			}
      			$i=0;
      			foreach ($stat['kecamatan'] as $k){
      				if ($k->nama==$d->kecamatan_nama){
      					$result['mkecamatan'] = $k;
      				}
      				if ($i<100){
      					array_push($result['kecamatan'],$k);
      				}
      				$i++;
      			}
      			$i=0;
      			foreach ($stat['kota'] as $k){
      				if ($k->nama==$d->kota_nama){
      					$result['mkota'] = $k;
      				}      				
      				if ($i<100){
      					array_push($result['kota'],$k);
      				}
      				$i++;
      			}
	   		}
	    }
        echo json_encode($result);
		return "";
	}

	public function news(){
		$data=array();
		$uid = Request::get("uid");
	    if ($uid!=null){
	    	if ($this->auth($uid)){
	    		$token = Request::get("access_token");
	    		$data = DB::table("wolo_post")->where("post_kategori",1)->where("post_status",1)->get();
      			
      			return view('news',compact("data","token"));
	    	}
	    }
      	return "Silahkan refresh!"; 
	}


	public function event(){
		$uid = Request::get("uid");
	    if ($uid!=null){
	    	if ($this->auth($uid)){
	    		$token = Request::get("access_token");
	    		$data = DB::table("wolo_post")->where("post_kategori",2)->where("post_status",1)->get();

      			return view('event',compact("data","token"));
	    	}
	    }
      	return "Silahkan refresh!"; 
	}

	public function post_show($id){
	    $data = DB::table("wolo_post")->where("post_status",1)->where("post_id",$id)->first();

      	return view('post_detail',compact("data"));
	}


	public function faqs(){
		$data=array();
		$uid = Request::get("uid");
	    if ($uid!=null){
	    	if ($this->auth($uid)){
	    		$data = DB::table("wolo_faqs")->where("faqs_status",1)->get();

      			return view('faqs',compact("data"));
	    	}
	    }
      	return "Silahkan refresh!"; 
	}
}
