<?php

namespace App\Http\Middleware;

use Request;
use Closure;
use Tymon\JWTAuth\Exceptions\JWTException;
use Tymon\JWTAuth\Exceptions\TokenExpiredException;
use Tymon\JWTAuth\Middleware\BaseMiddleware;

class AuthApi extends BaseMiddleware
{
    
    public function handle($request, Closure $next)
    {		
		if (! $token = Request::get("access_token")) {
            return response()->json(['exception'=> 'token_not_provided'], 200);
        }
		try {
            $user = $this->auth->authenticate($token);
        } catch (TokenExpiredException $e) {
            return response()->json(['exception'=> 'token_expired'], $e->getStatusCode());
        } catch (JWTException $e) {
            return response()->json(['exception'=> 'token_invalid'], $e->getStatusCode());
        }

        if (! $user) {
            return response()->json(['exception'=> 'user_not_found'], 200);
        }
		
		$this->events->fire('tymon.jwt.valid', $user);
		
		
		$request["uid"] = $user['client_secret'];
        return $next($request);
    }
}
