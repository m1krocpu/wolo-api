<?php

namespace App;

use Illuminate\Support\Facades\DB;

class Statistik
{
	
    public function run(){
		$data = array();
		$users = DB::table("users")->join("wolo_penduduk","penduduk_nik","akun_penduduk")
				->join("wolo_desa","desa_id","penduduk_desa")
				->join("wolo_kecamatan","kecamatan_id","desa_kecamatan")
				->join("wolo_kota","kota_id","kecamatan_kota")
				->get();
		$user = array();
		foreach($users as $u){
			$user[$u->username] = array(
				"desa"=>$u->desa_id,
				"kecamatan"=>$u->kecamatan_id,
				"kota"=>$u->kota_id,
			);
		}
		
		//$stat = DB::table("vw_statistik")->get();
		$stat = DB::table(DB::raw("(SELECT duel_id, duel_akun, IF(a_benar IS NULL, 0, a_benar) as score_akun, IF(IF(a_benar IS NULL, 0, a_benar)>IF(e_benar IS NULL, 0, e_benar),3,IF(IF(a_benar IS NULL, 0, a_benar)=IF(e_benar IS NULL, 0, e_benar),1,0)) as poin_akun, duel_enemy, IF(e_benar IS NULL, 0, e_benar) as score_enemy , IF(IF(a_benar IS NULL, 0, a_benar)<IF(e_benar IS NULL, 0, e_benar),3,IF(IF(a_benar IS NULL, 0, a_benar)=IF(e_benar IS NULL, 0, e_benar),1,0)) as poin_enemy FROM wolo_duel LEFT join (SELECT djawab_duel as a_id, djawab_akun as a_akun, count(*) as a_benar FROM `wolo_duel_jawab` join wolo_soal on djawab_soal=soal_id WHERE lower(djawab_isi) = lower(soal_jawab) group by djawab_duel, djawab_akun) d_ally on duel_id = a_id AND duel_akun=a_akun LEFT JOIN (SELECT djawab_duel as e_id, djawab_akun as e_akun, count(*) as e_benar FROM `wolo_duel_jawab` join wolo_soal on djawab_soal=soal_id WHERE lower(djawab_isi) = lower(soal_jawab) group by djawab_duel, djawab_akun) d_enemy on duel_id = e_id AND duel_enemy=e_akun where duel_enemy is not null) statistik"))->get();
		
		$data = array();
		foreach($stat as $s){
			$username = $s->duel_akun;
			if (isset($user[$username])){
				$u = $user[$username];			
				$item = array();
				$item['user'] = $username;
				$item['desa'] = $u['desa'];
				$item['kecamatan'] = $u['kecamatan'];
				$item['kota'] = $u['kota'];
				$item['poin'] = $s->poin_akun;
				array_push($data, $item);
			}
			
			$username = $s->duel_enemy;
				if (isset($user[$username])){
				$u = $user[$username];	
				$item = array();
				$item['user'] = $username;
				$item['desa'] = $u['desa'];
				$item['kecamatan'] = $u['kecamatan'];
				$item['kota'] = $u['kota'];
				$item['poin'] = $s->poin_enemy;
				array_push($data, $item);
			}
		}
		
		$tmp['kelurahan'] = array();
		$tmp['kecamatan'] = array();
		$tmp['kota'] = array();

		$desa = DB::table("wolo_desa")->get();
		foreach($desa as $r){
			$item = array();
			$item['nama'] = $r->desa_nama;
			$item['menang']=0;
			$item['seri']=0;
			$item['kalah']=0;
			$item['poin']=0;
			foreach($data as $d){
				if ($d['desa']==$r->desa_id){
					$item['poin']+=$d['poin'];
					if ($d['poin']==3){
						$item['menang']++;
					}else if ($d['poin']==1){
						$item['seri']++;
					}else if ($d['poin']==0){
						$item['kalah']++;
					}
				}
			}
			array_push($tmp['kelurahan'],$item);
		}

		$kecamatan = DB::table("wolo_kecamatan")->get();
		foreach($kecamatan as $r){
			$item = array();
			$item['nama'] = $r->kecamatan_nama;
			$item['menang']=0;
			$item['seri']=0;
			$item['kalah']=0;
			$item['poin']=0;
			foreach($data as $d){
				if ($d['kecamatan']==$r->kecamatan_id){
					$item['poin']+=$d['poin'];
					if ($d['poin']==3){
						$item['menang']++;
					}else if ($d['poin']==1){
						$item['seri']++;
					}else if ($d['poin']==0){
						$item['kalah']++;
					}
				}
			}
			array_push($tmp['kecamatan'],$item);
		}

		$kota = DB::table("wolo_kota")->get();
		foreach($kota as $r){
			$item = array();
			$item['nama'] = $r->kota_nama;
			$item['menang']=0;
			$item['seri']=0;
			$item['kalah']=0;
			$item['poin']=0;
			foreach($data as $d){
				if ($d['kota']==$r->kota_id){
					$item['poin']+=$d['poin'];
					if ($d['poin']==3){
						$item['menang']++;
					}else if ($d['poin']==1){
						$item['seri']++;
					}else if ($d['poin']==0){
						$item['kalah']++;
					}
				}
			}
			array_push($tmp['kota'],$item);
		}
		
		uasort($tmp['kelurahan'], function($a, $b) {
		    if ($a['poin'] == $b['poin']) {
		        return 0;
		    }
		    return ($a['poin'] < $b['poin']) ? 1 : -1;
		});

		uasort($tmp['kecamatan'], function($a, $b) {
		    if ($a['poin'] == $b['poin']) {
		        return 0;
		    }
		    return ($a['poin'] < $b['poin']) ? 1 : -1;
		});

		uasort($tmp['kota'], function($a, $b) {
		    if ($a['poin'] == $b['poin']) {
		        return 0;
		    }
		    return ($a['poin'] < $b['poin']) ? 1 : -1;
		});

		
		$result['kelurahan'] = array();
		$result['kecamatan'] = array();
		$result['kota'] = array();

		$rank=1;
		foreach($tmp['kelurahan'] as $r){
			$item = $r;
			$item['rank'] = $rank;
			$item['kategori'] = "kelurahan";
			array_push($result['kelurahan'], $item);

			$rank++;
		}


		$rank=1;
		foreach($tmp['kecamatan'] as $r){
			$item = $r;
			$item['rank'] = $rank;
			$item['kategori'] = "kecamatan";
			array_push($result['kecamatan'], $item);
			
			$rank++;
		}


		$rank=1;
		foreach($tmp['kota'] as $r){
			$item = $r;
			$item['rank'] = $rank;
			$item['kategori'] = "kota";
			array_push($result['kota'], $item);
			
			$rank++;
		}

		DB::table("wolo_statistik")->delete();
		DB::table("wolo_statistik")->insert($result['kelurahan']);
		DB::table("wolo_statistik")->insert($result['kecamatan']);
		DB::table("wolo_statistik")->insert($result['kota']);
		
	}
	
}
